<?php
  header('Content-Type: text/html; charset=utf-8');

  if (!ini_get('date.timezone')) {
	  date_default_timezone_set('Europe/Paris');
  }
  
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);


  require_once 'feed.php';

  $rssSonic = Feed::loadRss('https://shaarli.kazhnuz.space/feed/rss?&searchtags=Sonic');
  $rssSEGA = Feed::loadRss('https://shaarli.kazhnuz.space/feed/rss?&searchtags=SEGA');
?>

<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Press Garden Zone</title>
    <link rel="stylesheet" href="./iframe.css">
    <link rel="icon" href="./favicon.png">
  </head>
  <body>
    <main>
      <?php $i = 1; ?>
      <?php foreach ($rssSonic->item as $item): ?>
        <?php if ($i <= 6) { ?>
          <article>
            <div><a href="/#news-sonic-<?php echo $i ?>" target="_blank"><?php echo htmlspecialchars($item->title) ?></a></div>
            <div><small><?php echo date('j.n.Y H:i', (int) $item->timestamp) ?></small></div>
          </article>
        <?php $i++; ?>
        <?php } ?>
      <?php endforeach ?>
    </main>
  </body>
</html>
