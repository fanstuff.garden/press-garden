<?php
  header('Content-Type: text/html; charset=utf-8');

  if (!ini_get('date.timezone')) {
	  date_default_timezone_set('Europe/Paris');
  }
  
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);


  require_once 'feed.php';

  $rssSonic = Feed::loadRss('https://shaarli.kazhnuz.space/feed/rss?&searchtags=Sonic');
  $rssSEGA = Feed::loadRss('https://shaarli.kazhnuz.space/feed/rss?&searchtags=SEGA');

  $showSEGA = false;
  if ($_GET != null && $_GET["show"] == "sega") {
    $showSEGA = true;
  }
?>

<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Press Garden Zone</title>
    <link rel="stylesheet" href="./style.css">
    <link rel="icon" href="./favicon.png">
  </head>
  <body>
    <header>
      <nav>
        <ul>
          <li><a href="#" <?php if ($showSEGA == false) { ?> class="active" <?php } ?> id="link-sonic">Sonic</a></li>
          <li><a href="#" <?php if ($showSEGA == true ) { ?> class="active" <?php } ?> id="link-sega">SEGA</a></li>
        </ul>
      </nav>
    </header>
    <main>
      <h1 id="head">Press Garden Zone</h1>
      <section <?php if ($showSEGA == false) { ?> class="shown" <?php } ?> id="section-sonic">
        <?php $i = 1; ?>
        <?php foreach ($rssSonic->item as $item): ?>
          <?php if ($i <= 12) { ?>
            <article id="news-sonic-<?php echo $i; ?>">
	            <h2><a href="<?php echo htmlspecialchars($item->url) ?>"><?php echo htmlspecialchars($item->title) ?></a>
	            <small><?php echo date('j.n.Y H:i', (int) $item->timestamp) ?></small></h2>
	            <div><?php echo $item->description ?></div>
	          </article>
          <?php $i++; ?>
	        <?php } ?>
        <?php endforeach ?>
        <div class="btn-area"><a class="btn" href="https://shaarli.kazhnuz.space/?searchtags=Sonic"> Plus ancien articles </a></div>
      </section>
      <section <?php if ($showSEGA == true ) { ?> class="shown" <?php } ?> id="section-sega">
        <?php $i = 1; ?>
        <?php foreach ($rssSEGA->item as $item): ?>
          <?php if ($i <= 12) { ?>
          <article id="news-sega-<?php echo $i; ?>">
	          <h2><a href="<?php echo htmlspecialchars($item->url) ?>"><?php echo htmlspecialchars($item->title) ?></a>
	          <small><?php echo date('j.n.Y H:i', (int) $item->timestamp) ?></small></h2>
	          <div><?php echo $item->description ?></div>
	        </article>
          <?php $i++; ?>
	        <?php } ?>
        <?php endforeach ?>
        <div class="btn-area"><a class="btn" href="https://shaarli.kazhnuz.space/?searchtags=SEGA"> Plus ancien articles </a></div>
      </section>
    </main>
    <footer>
      <p>Press Garden est une petite page servant juste à afficher les flux RSS Sonic et SEGA du <a href="https://shaarli.kazhnuz.space/">shaarli de Kazhnuz</a>.</p>
      <p>Ce site est hébergé par Fanstuff Garden.</p>
      <p><a href="https://fanstuff.garden"><img src="assets/fanstuffgarden.gif" /></a></p>
    </footer>
	<script>
    const sectionSonic = document.getElementById('section-sonic');
    const linkSonic = document.getElementById('link-sonic');
    const sectionSEGA = document.getElementById('section-sega');
    const linkSEGA = document.getElementById('link-sega');
	  linkSonic.addEventListener('click', function () {
	    linkSEGA.classList.remove('active');
	    linkSonic.classList.add('active');
	    
	    sectionSEGA.classList.remove('shown');
	    sectionSonic.classList.add('shown');
    });
	  linkSEGA.addEventListener('click', function () {
	    linkSEGA.classList.add('active');
	    linkSonic.classList.remove('active');
	    
	    sectionSEGA.classList.add('shown');
	    sectionSonic.classList.remove('shown');
    });
	</script>
  </body>
</html>
